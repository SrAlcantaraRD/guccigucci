# GucciGucci

Registro del proceso de adquisición de habilidades de ™Gucci#8241

---

## Objetivo

Completar el curso de Javascript (TypeScript), los ejercicios y resolver el mismo problema pero con distinto [Patrón de Diseño](https://refactoring.guru/es/design-patterns), evitando los [Code Smells](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files.

## Cómo lo hará ™Gucci#8241

1. Por cada lección, creará un ticket [issue](https://gitlab.com/SrAlcantaraRD/guccigucci/-/issues) en el repositorio.

2. Del ticket, creará una rama con el nombre de la lección.

3. En la rama, creará un archivo .ts por cada ejercicio.

4. Al completar cada ejercicio, hará un commit siguiendo una estructura definida.

5. Pushará el cambio en la rama y creará un PR.

## Entorno de Aprendizaje

1. Arrancar el entorno de compilación: ejecutar `"yarn watch"`
2. Compilación caliente: ejecutar `"yarn start"`
